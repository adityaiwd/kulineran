Projek Vue Pemrograman Web Lanjut
# Kulineran

## How To Use

### Step 1

- 👯 Clone this repo to your local machine using 
  ```shell
  git clone https://gitlab.com/adityaiwd/kulineran.git
  ```
  after cloning this repo, don't forget to install all of the dependencies using
  ```shell
  npm install
  ```


### Step 2

- run the backend json-server
```shell
json-server --watch backend/db.json
```

### Step 3

- run it on your local server using
```shell
npm run serve
```
